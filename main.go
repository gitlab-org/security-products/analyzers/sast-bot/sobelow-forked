package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v5/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v5/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v5/plugin"
)

func main() {
	app := command.NewApp(metadata.AnalyzerDetails)

	app.Commands = command.NewCommands(command.Config{
		Match:             plugin.Match,
		Analyze:           analyze,
		Analyzer:          metadata.AnalyzerDetails,
		AnalyzeFlags:      analyzeFlags(),
		Convert:           convert.Convert,
		Scanner:           metadata.ReportScanner,
		ScanType:          metadata.Type,
		LoadRulesetConfig: loadRulesetConfig,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
