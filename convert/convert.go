package convert

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v5/metadata"
)

// Convert reads a native report from the tool (sobelow) and transforms it
// into issues (vulnerabilities), as defined in the common module
func Convert(reader io.Reader, _ string, rulesetConfig *ruleset.Config) (*report.Report, error) {
	var fileReport SoBelowReport

	err := json.NewDecoder(reader).Decode(&fileReport)
	if err != nil {
		return nil, err
	}

	// Create vulns from sobelow report
	vulns := make([]report.Vulnerability, 0)

	for _, vuln := range fileReport.vulnerabilities() {
		description, ok := vuln.description()
		if !ok {
			// Ignore unknown rule types
			continue
		}

		newVuln := report.Vulnerability{
			Category:    metadata.Type,
			Scanner:     &metadata.IssueScanner,
			Name:        vuln.message(),
			Description: description,
			CompareKey:  vuln.compareKey(),
			Severity:    report.SeverityLevelUnknown,
			Confidence:  vuln.Confidence,
			Location: report.Location{
				File:      vuln.File,
				LineStart: vuln.Line,
			},
			Identifiers: []report.Identifier{
				{
					Type:  "sobelow_rule_id",
					Name:  fmt.Sprintf("Sobelow Rule ID %s", vuln.typeSlug()),
					Value: vuln.typeSlug(),
				},
			},
		}

		vulns = append(vulns, newVuln)
	}

	newReport := report.NewReport()
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = vulns
	newReport.FilterDisabledRules(rulesetConfig)
	return &newReport, nil
}
