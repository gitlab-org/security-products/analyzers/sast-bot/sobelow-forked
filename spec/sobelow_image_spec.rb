require "tmpdir"
require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def project_dir
     '/app'
  end

  def image_name
    ENV.fetch('TMP_IMAGE', 'sobelow:latest')
  end

  context 'with no project' do
   let(:output) { `docker run -t --rm --user=gitlab -w #{project_dir} #{image_name}` }
   let(:exit_code) { $CHILD_STATUS.to_i }

    it 'shows there is no match' do
      expect(output).to match(/no match in #{project_dir}/i)
    end

    describe 'exit code' do
      specify { expect(exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = 'gl-sast-report.json')
      path = File.join(expectations_dir, expectation_name, report_name)
      JSON.parse(File.read(path))
    end

    let(:secure_log_level) { 'debug' }
    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        # CI_PROJECT_DIR env var value is referred by the Analyzer(1) and
        # Post Analyzer script(2) to determine the target directory for
        # performing any action.
        #
        # References:
        # (1): https://gitlab.com/gitlab-org/security-products/analyzers/command/blob/6eb84053950dcf4c06b768484880f9d3d9aebde1/run.go#L132-132
        # (2): https://gitlab.com/gitlab-org/security-products/post-analyzers/scripts/-/blob/25479eae03e423cd67f2493f23d0c4f9789cdd0e/start.sh#L2
        'CI_PROJECT_DIR': project_dir, 
        'SECURE_LOG_LEVEL': secure_log_level
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        user: 'gitlab',
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context "with phoenix framework" do
      let(:project) { "elixir-phoenix/default" }
      let(:variables) do
          { 'GITLAB_FEATURES': 'vulnerability_finding_signatures' }
      end

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like "valid report"
      end
    end

    context 'with rules disabled' do
      let(:project) { "elixir-phoenix/with-rules-disabled" }
      let(:variables) do
          { 'GITLAB_FEATURES': 'vulnerability_finding_signatures, sast_custom_rulesets' }
      end

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like "valid report"
      end
    end

    context 'with warning logs' do
      let(:project) { "elixir-phoenix/with-warning-logs" }

      context 'when testing sobelow log output' do
        context 'when SECURE_LOG_LEVEL is set to debug' do
          it 'outputs debug log messages' do
            expect(scan.combined_output).to match(/\[DEBU\] \[Sobelow\] \[.*\] \[.*\] ▶ Applying report overrides/)
          end


          it 'streams the output of the sobelow command at the info level' do
            expect(scan.combined_output).to match(/\[INFO\] \[Sobelow\] \[.*\] ▶ WARNING: Sobelow cannot find the router. If this is a Phoenix application/)
          end
        end

        context 'when SECURE_LOG_LEVEL is not set' do
          let(:secure_log_level) { nil }

          it 'does not output debug log messages' do
            expect(scan.combined_output).not_to match(/\[DEBU\] \[Sobelow\] \[.*\] \[.*\] ▶ Applying report overrides/)
          end

          it 'streams the output of the sobelow command at the info level' do
            expect(scan.combined_output).to match(/\[INFO\] \[Sobelow\] \[.*\] ▶ WARNING: Sobelow cannot find the router. If this is a Phoenix application/)
          end
        end
      end
    end
  end
end
