package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
)

// Match checks a filename and makes sure it is mix.exs
func Match(_ string, info os.FileInfo) (bool, error) {
	if info.Name() == "mix.exs" {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("sobelow", Match)
}
