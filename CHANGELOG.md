# Sobelow SAST analyzer changelog

## v5.11.1
- Update `report` package from `v5.5.0` to `v5.7.0` (!136)
- Downgrade security report schema from `15.2.1` to `15.1.4` to fix regression. See https://gitlab.com/gitlab-org/gitlab/-/issues/497405#note_2348142562 (!136)

## v5.11.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.5.0` => [`v5.6.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.6.0)] (!135)

## v5.10.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.3.0` => [`v5.5.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.5.0)] (!134)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.2.1` => [`v3.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.3.1)] (!134)

## v5.9.0
- always stream output from scanner using `info` log level (!132)

## v5.8.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.4` => [`v2.27.5`](https://github.com/urfave/cli/releases/tag/v2.27.5)] (!129)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.3.0` => [`v3.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.4.0)] (!129)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.1` => [`v3.2.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.2.1)] (!129)

## v5.7.0
- Add support for disabling predefined rules via custom rulesets (!127)

## v5.6.0
- Add non-root user (gitlab, uid=1000, gid=1000) (!125)

## v5.5.0
- Upgrade base image to Elixir 1.14 (https://gitlab.com/gitlab-org/gitlab/-/issues/424530) (!105)

## v5.4.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.3` => [`v2.27.4`](https://github.com/urfave/cli/releases/tag/v2.27.4)] (!123)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.2.1` => [`v5.3.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.3.0)] (!123)

## v5.3.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.3` => [`v3.3.0`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.3.0)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.0` => [`v3.1.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.1.1)] (!121)

## v5.2.0
- upgrade [`Jason`](https://github.com/michalmuskala/jason) version [`1.4.1` => `1.4.4`] (!120)
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.3`](https://github.com/urfave/cli/releases/tag/v2.27.3)] (!120)

## v5.1.0
- Update `command` package from `v2.2.0` to `v3.1.0` (!113)
- Update `report` package from `v4.3.2` to `v5.2.1` (!113)
- Update `ruleset` package from `v1.4.1` to `v3.1.0` (!113)
- Update version of sobelow module from `v2` to `v5` (!113)

## v5.0.1
- update `go` version to `v1.22.3` (!119)

## v5.0.0
- Bump to next major version (!114)

## v4.2.7
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!112)

## v4.2.6
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!111)

## v4.2.5
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!110)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!110)

## v4.2.4
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!109)

## v4.2.3
- upgrade `github.com/google/go-cmp` version [`v0.5.9` => [`v0.6.0`](https://github.com/google/go-cmp/releases/tag/v0.6.0)] (!108)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!108)

## v4.2.2
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!107)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!107)

## v4.2.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!106)

## v4.2.0
- Upgrade base image to Elixir 1.13 (https://gitlab.com/gitlab-org/gitlab/-/issues/410169) (!104)

## v4.1.0
- upgrade [`Sobelow`](https://github.com/nccgroup/sobelow) version [`0.12.2` => `0.13.0`] (!101)
    - Removes support for Elixir 1.5 and 1.6
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!101)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.3` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!101)

## v4.0.3
- upgrade [`Jason`](https://github.com/michalmuskala/jason) version [`1.4.0` => `1.4.1`] (!99)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.5` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!99)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!99)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.3`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.3)] (!99)

## v4.0.2
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (gitlab-org/security-products/analyzers/sobelow!98)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.5`](https://github.com/urfave/cli/releases/tag/v2.25.5)] (gitlab-org/security-products/analyzers/sobelow!98)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (gitlab-org/security-products/analyzers/sobelow!98)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (gitlab-org/security-products/analyzers/sobelow!98)

## v4.0.1
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!97)

## v4.0.0
- Bump to next major version (!96)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!96)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!96)

## v3.2.10
- upgrade [`Sobelow`](https://github.com/nccgroup/sobelow) version [`0.11.1` => `0.12.2`] (!95)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.0` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!95)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!95)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!95)

## v3.2.9
- upgrade `github.com/urfave/cli/v2` version [`v2.24.3` => [`v2.25.0`](https://github.com/urfave/cli/releases/tag/v2.25.0)] (!94)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!94)
- Update Go to 1.19 (!94)

## v3.2.8
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.24.3`](https://github.com/urfave/cli/releases/tag/v2.24.3)] (!93)

## v3.2.7
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!92)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!92)

## v3.2.6
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!90)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!90)

## v3.2.5
- Ensure `git` is installed in the Docker image (!89)

## v3.2.4
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!88)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!88)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.2` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!88)

## v3.2.3
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!87)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!87)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!87)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.15.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.2)] (!87)

## v3.2.2
- Update common to `v3.2.1` to fix gotestsum cmd (!86)

## v3.2.1
- upgrade [`Jason`](https://github.com/michalmuskala/jason) version [`1.3.0` => `1.4.0`] (!85)
- upgrade `github.com/google/go-cmp` version [`v0.5.8` => [`v0.5.9`](https://github.com/google/go-cmp/releases/tag/v0.5.9)] (!85)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)] (!85)

## v3.2.0
- upgrade [`Jason`](https://github.com/michalmuskala/jason) version [`1.2.2` => `1.3.0`] (!83)
- upgrade `github.com/google/go-cmp` version [`v0.5.6` => [`v0.5.8`](https://github.com/google/go-cmp/releases/tag/v0.5.8)] (!83)
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!83)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!83)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.0` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!83)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!83)

## v3.1.0
- Upgrade core analyzer dependencies (!78)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.0
- Bumping to `v3.0.0` (!76)

## v2.15.0
- Update ruleset, report, and command modules to support ruleset overrides (!73)

## v2.14.2
- Update internal dependencies (!70)
  - Report output will now be optimized

## v2.14.1
- chore: Update go to v1.17 (!69)

## v2.14.0
- Enable vulnerabilities in `Config` module (!67 @rbf)

## v2.13.0
- Add URL references to the Sobelow moduledocs from where the vulnerability
  descriptions have been extracted (!65 @rbf)
- Make all vulnerability descriptions consistent with the source referenced (!65 @rbf)

## v2.12.2
- Fix vulnerabilities found by gosec (!62)

## v2.12.1
- Update Dockerfile to restrict sobelow permissions (!54)

## v2.12.0
- Update Dockerfile to support openshift (!53)

## v2.11.0
- Update `report` dependency (!52)
  - Update schema version to `14.0.0`

## v2.10.0
- Update sobelow to v0.11.1 (!50)
  - Bugfix: Command Injection finding description are properly formatted
  - Misc: Custom JSON serialization replaced with Jason.

## v2.9.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!49)

## v2.9.0
- Update common to v2.22.0 (!48)
- Update urfave/cli to v2.3.0 (!48)

## v2.8.0
- Update elixir elixir to v1.11 (!45)
- Update sobelow to v0.10.6
- update logrus and cli golang dependencies to latest versions

## v2.7.2
- Update common and enabled disablement of rulesets (!44)

## v2.7.1
- Update golang dependencies (!38)

## v2.7.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!35)

## v2.6.1
- Upgrade go to version 1.15 (!34)

## v2.6.0
- Add scan object to report (!30)

## v2.5.1
- Bump sobelow to [v0.10.4](https://github.com/nccgroup/sobelow/blob/master/CHANGELOG.md#v0104) (!28)
- Bump elixir base image to 1.10.4

## v2.5.0
- Switch to the MIT Expat license (!26)

## v2.4.1
- Update Debug output to give a better description of command that was ran (!25)

## v2.4.0
- Update logging to be standardized across analyzers (!24)

## v2.3.4
- Bump sobelow to v0.10.3 (!23)

## v2.3.3
- Remove `location.dependency` from the generated SAST report (!22)

## v2.3.2
- Upgrade elixir base image to 1.10.3 (!21)
- Upgrade to [Sobelow 0.10.2](https://github.com/nccgroup/sobelow/blob/master/CHANGELOG.md#v0102)

## v2.3.1
- Use Alpine as builder image (!17)

## v2.3.0
- Add `id` field to vulnerabilities in JSON report (!16)

## v2.2.0
- Add support for custom CA certs (!14)

## v2.1.1
- Upgrade elixir base image to 1.9.4

## v2.1.0
- Upgrade to [Sobelow 0.8.0](https://github.com/nccgroup/sobelow/blob/master/CHANGELOG.md#v080)
- Fix link to affected line of the source code
- Change compare keys, use fixed line number and remove function name

## v2.0.1
- Define MIX_HOME in Dockerfile to prevent dynamic definition at runtime

## v2.0.0
- Initial release
